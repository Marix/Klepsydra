/**
 * @file
 *
 * Smoke tests for the monotonic timer.
 *
 * Copyright 2011 Matthias Bach <marix@marix.org>
 *
 * This file is part of Klepsydra.
 *
 * Klepsydra is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Klepsydra is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Klepsydra.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <unistd.h>

#include <klepsydra/klepsydra.hpp>

using namespace std;
using namespace klepsydra;

// this test only ensures that the doce doesn't crash

int main( int, char ** )
{
	Monotonic timer;

	cout << "Immediately : " << timer.getTime() << " mus" << endl;

	sleep( 1 );

	cout << "1s          : " << timer.getTime() << " mus" << endl;

	sleep( 10 );
	
	cout << "11s         : " << timer.getTimeAndReset() << " mus" << endl;
	
	sleep( 5 );
	
	cout << "5s           : " << timer.getTime() << " mus" << endl;
	
	sleep( 5 );

	timer.reset();
	
	sleep( 5 );
	
	cout << "5s           : " << timer.getTime() << " ms" << endl;
}

