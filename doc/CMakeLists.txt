# This file is part of Klepsydra.
#
# clRand is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# clRand is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with clRand.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2011 Matthias Bach <bach@compeng.uni-frankfurt.de>

set(DOXYGEN_LANGUAGE "English" CACHE STRING "Language used by doxygen")
mark_as_advanced(DOXYGEN_LANGUAGE)

set(DOXYGEN_SOURCE_DIRS
    ${CMAKE_SOURCE_DIR}
    ${CMAKE_SOURCE_DIR}/include/klepsydra/
    )
set(DOXYGEN_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR})

string(REGEX REPLACE ";" " " DOXYGEN_INPUT "${DOXYGEN_SOURCE_DIRS}")
configure_file(Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

set(HTML_TARGET ${DOXYGEN_OUTPUT_DIR}/html/index.html)
add_custom_target(doc
                  COMMAND ${DOXYGEN} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
                  DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

